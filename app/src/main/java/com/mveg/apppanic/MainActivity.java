package com.mveg.apppanic;
/*Resumen en la MainActivity esta todo el desmadre
* podemos ver que se creo una clase llamada SimpleDialog
* esta clase es un dialogo que se inicia al cargar la app(se explica mas adelante).
* la MainActivity implementa dos interfaces una propia y la otra Location Listener
*
* */

//import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.Toast;
import android.location.LocationListener;
import java.util.List;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements SimpleDialog.DialogData, LocationListener {
    SharedPreferences preferences;//este objeto se usa para almacenar datos en la app
    SharedPreferences.Editor editor;//este es el que guardara los datos en la madre de arriba XD
    LocationManager manager;//es el encargado de las locaciones lol
    ImageButton panic;// ya te imaginaras que es esto
    Location location;//este es un objeto que contiene latitud y longitud
    String proveedor; // cade que mas adelante se explica
    Criteria criteria;//esta igual que la anterior
    double latitud, longitud;//estas creo sobran
    SmsManager smsManager;//este es el encargado de los SMS

    //@TargetApi(Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //inicializamos los componentes
        smsManager = SmsManager.getDefault();//como si no supieras que hace esta linea XD
        //aqui inicializamos las preferencias donde R.string.KKEY_PREFERENCES es una clave que se le dara
        //al archivo de las preferencias y context.mode_private indica que solo esta app usara ese achivo
        preferences = getSharedPreferences(getString(R.string.KEY_PREFERENCES), Context.MODE_PRIVATE);
        //jaja sin comentarios
        manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        //criteria este objeto sirve para obtener un mejor critrio a la hora de buscar los proveedores de localizacion
        criteria = new Criteria();
        //para esto servia el proveedor XD jaja guardamos el mejor proveedor que encontremos
        proveedor = manager.getBestProvider(criteria,false);
        //y esta mas que literal que hace esta instruccion
        location = manager.getLastKnownLocation(proveedor);
        //inicializamos el editor de preferencias
        editor = preferences.edit();
        //le ponemos una preferencia(DIALOS_ESTADO = 0)
        int estado = preferences.getInt(getString(R.string.KEY_ESTADO_DIALOG), 0);
        //Esto se ejecutara la primera vez que inicie la app
        //como es la primera vez DIALOG_ESTADO es cero y aparece
        //el dialogo
        //despues a DIALOG le asignamos un uno y ya no aparece nunca jamas D: haha
        if (estado == 0) {
            createDialog();
            editor.putInt(getString(R.string.KEY_ESTADO_DIALOG), 1);
            editor.commit();//yo sirvo para guardar los cambios en las preferencias
        }
        //bitch please XD jajaja ya sabes que hace esto
        panic = (ImageButton) findViewById(R.id.bt_panic);
        //y esto tambien
        panic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //aqui extraemos el numero que almacenamos, en caso de que no tenga nada pone 0000
                String number = preferences.getString(getString(R.string.KEY_NUMERO),"0000");
                if(number.equals("0000")){
                    //si son 000 muestra un mensaje bueno esto se puede omitir XD
                    Toast.makeText(MainActivity.this, number, Toast.LENGTH_SHORT).show();
                }else{
                    //y se crea el mensaje se le agrega la direccion
                    //la segunda linea igual se puede omitir
                    String mensaje = "Estoy en apuros y me encuentroo en esta direccion " + getDireccion(location);
                    Toast.makeText(MainActivity.this,"Numero: "+number+"\nmensaje :"+mensaje, Toast.LENGTH_LONG).show();
                    sendSMS(number, mensaje);//yo mando el mensaje
                }

                //Toast.makeText(MainActivity.this, "latitud: "+ latitud+"\nlongitud: "+longitud, Toast.LENGTH_SHORT).show();
                //Toast.makeText(MainActivity.this,getDireccion(location),Toast.LENGTH_LONG).show();
            }
        });

    }

    private void createDialog() {
        //Aqui solo inicializamos el objeto dialog que es el dialogo que se muestra
        SimpleDialog dialog = new SimpleDialog();
        dialog.show(getSupportFragmentManager(), "SimpleDialog");//yo lo muestro
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            createDialog();//y aqui vuelvo a aparecer cuando presionas settings muajaja
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    //este metodo es de la interface DialogAlgoNoSeQuePincheNombreLePuse XD jaja
    //sirve para traer los datos del dialog
    @Override
    public void onPositiveButon(String s) {
        saveSharedPreferences(s);//yo guardo los datos mejor dicho el dato del dialog en las preferencias
        Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();//yo puedo ser omitido u.u
    }

    public void saveSharedPreferences(String data) {
        //SharedPreferences preferences = getSharedPreferences(getString(R.string.KEY_PREFERENCES),Context.MODE_PRIVATE);
        editor = preferences.edit();//ya vimos que hago yo
        editor.putString(getString(R.string.KEY_NUMERO), data);//y yo jaja solo que ahora pongo una cadena no un entero
        editor.commit();//y yo de nuevo XD jaja yo los guardo
    }

    @Override
    protected void onPause() {
        super.onPause();
        manager.removeUpdates(this);//quito las actualizaciones de locaciones
    }

    @Override
    protected void onResume() {
        super.onResume();
        manager.requestLocationUpdates(proveedor, 100, 5, this);//yo busco mas locaciones cada 100milisegundos y cada 5 metros
    }
    //nosotros somos metodos de locationListener
    @Override
    public void onLocationChanged(Location location) {
        //este metodo cada vez que cambia de locacion se ejecuta
        latitud = location.getLatitude();
        longitud = location.getLongitude();
        //this.location = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        //yo no hago nada, cada que que hay un cambio no se de que entro en accion
    }

    @Override
    public void onProviderEnabled(String provider) {
        //yo entro en accion cuando algun proveedor esta disponible
    }

    @Override
    public void onProviderDisabled(String provider) {
        //!onProviderEnabled jaja ok no yo hago lo contratio del anterior
    }
    //este metodo obtiene la direccion de la locacion que le mandas
    //(como si el nombre no lo digera XD)
    private String getDireccion(Location l){
        if(l.getLatitude() != 0 && l.getLongitude() != 0){//validamos que las coordenadas tengan algo
            try{
                Geocoder geo = new Geocoder(this,Locale.getDefault());//no se muy bien que sea esto, pero funciona
                List<Address> list = geo.getFromLocation(l.getLatitude(),l.getLongitude(),1);//agregamos una lista de Address con un resultado
                if(!list.isEmpty()){//si no esta vacia
                    Address address = list.get(0);//extraigo la direccion
                    return address.getAddressLine(0);//y la retorno
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return "";
    }
    //esta cosa es obvia XD
    private void sendSMS(String number,String mensaje){
        if(smsManager==null){//vemos si nes nulo
            Toast.makeText(MainActivity.this, "Algo ha ocurrido", Toast.LENGTH_SHORT).show();//se cambia u omite
        }else{
            try{
                //si no es nulo entra en accion
                smsManager.sendTextMessage(number,null,mensaje,null,null);//yo REALMENTE MANDO EL MENSAJE jaja
                Toast.makeText(MainActivity.this, "Mensaje Enviado (:", Toast.LENGTH_SHORT).show();//omitir
            }catch (Exception e){
                Toast.makeText(MainActivity.this, "Mensaje no enviado", Toast.LENGTH_SHORT).show();//omitir
            }
        }
    }
}
