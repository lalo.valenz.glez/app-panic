package com.mveg.apppanic;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.util.zip.Inflater;

/**
 * Yo creo el cuadro de dialogo
 * extiendo de DialogFragment
 * soy un dialogo personalizado
 * mi unico atributo es un EditText
 */
public class SimpleDialog extends DialogFragment {
    EditText text;//yo soy el atributo
    //esta es la interfaz propia que enviara lo que tiene el text al MainActivity
    public interface DialogData{
        void onPositiveButon(String s);
    }

    DialogData data;//yo tambien soy atributo de la interfaz
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //este metodo se crea ejecuta cuando se crea el dialog
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());//este objeto es el que contruye el dialog
        builder.setTitle("Informacion");//con el titulo de informacion
        LayoutInflater inflater = getActivity().getLayoutInflater();//este sirve para amm inflar layouts
        View v = inflater.inflate(R.layout.simple_dialog,null);//se le dice cual va a inflar
        text = (EditText)v.findViewById(R.id.et_no_contacto);//enlazamos el text
        builder.setView(v)//le pasamos el view
                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //este metodo es como onClickListener
                        //Aqui es donde data le envia la data al ActivityMain jaja data envia data XD
                        data.onPositiveButon(text.getText().toString());
                    }
                });
        return builder.create();// y retorna su creacion muajaja
    }
    //on Attach sirve para no se que pedo con la implementacion de la interfaz creada anteriomente
    //si no la implementamos hay pedo
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            data = (DialogData) activity;
        }catch (ClassCastException e){
            e.printStackTrace();
        }
    }
}
